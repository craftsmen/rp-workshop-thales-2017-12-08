const { almostEqual } = require('../../util/utils');

module.exports = class Triangle {

	constructor(name, base, height) {
		this.name = name;
		this.base = base;
		this.height = height;
	}

	calculateArea() {
		return this.base * this.height / 2;
	}

	calculateCircumference() {
		return this.base + this.height + Math.sqrt(this.base * this.base + this.height * this.height);
	}

	getName() {
		return this.name;
	}

	equals(other) {
		return (
			other instanceof Triangle &&
			this.name === other.name &&
			almostEqual(this.base, other.base) &&
			almostEqual(this.height, other.height)
		);
	}

	toString() {
		return `Triangle(name='${this.name}', base=${this.base}, height=${this.height})`;
	}
}
