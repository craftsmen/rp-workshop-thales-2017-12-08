const Rx = require('rxjs/Rx');
const isPrime = require('../../util/utils').isPrime;
const number$ = require('../../util/example-streams.js').number$;
const checkSolution = require('../../util/solution-checker').checkSolution;

// ASSIGNMENT: Use to number$ to define a new stream that only contains those numbers that are prime.
//
// HINT: You can make use of the utility function isPrime to check if a given number is a prime number.

const primeNumber$ = null; // ???

// If implemented correctly, the application will output the following numbers: 1, 7, 2, 2, 7, 3

checkSolution('1-04', primeNumber$);
